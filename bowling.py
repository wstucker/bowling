# coding: utf-8
import pandas as pd
import matplotlib
matplotlib.use("SVG")
import matplotlib.pyplot as plt


MARKERS = ['o', 'x', '+', 'v', '^', '<', '>', 's', 'd']
BASIS_SCORE = 200
PERCENTAGE_FACTOR = 0.8

NUM_CHARTS = 7

bd = pd.read_csv('scores.csv', parse_dates=['Date'])

bowlers = bd['Bowler'].unique()

plt.figure("Bowling!")
plt.subplot(NUM_CHARTS, 1, 1)
plt.title("Scores")
plt.subplot(NUM_CHARTS, 1, 2)
plt.title("Cumulative Average")
plt.subplot(NUM_CHARTS, 1, 3)
plt.title("Average (Last 10 Games)")
plt.subplot(NUM_CHARTS, 1, 4)
plt.title("Handicap")
plt.subplot(NUM_CHARTS, 1, 5)
plt.title("Standard Deviation by Day")
plt.subplot(NUM_CHARTS, 1, 6)
plt.title("Average by Day")
plt.subplot(NUM_CHARTS, 1, 7)
plt.title("Max by Day")

def bprint(bowler, avg, best, last_10, handicap):
    print "|%s|%s|%s|%s|%s|" % (bowler, avg, best, last_10, handicap)

bprint("Bowler", "Avg", "Best", "Last 10", "Handicap")
bprint(":---", "---:", "---:", "---:", "---:")
    
for bowler, marker in zip(bowlers, MARKERS):
    b_games = bd.loc[bd.Bowler == bowler, ['Date', 'Game', 'Score']].sort_values(by=['Date', 'Game'])
    avg = b_games['Score'].mean()

    b_games['ExMean'] = b_games['Score'].expanding(min_periods=1).mean()
    b_games['RollingAvg10'] = b_games['Score'].rolling(window=10, min_periods=1).mean()
    b_games['Handicap'] = (BASIS_SCORE - b_games['RollingAvg10']) * PERCENTAGE_FACTOR

    dates = b_games.groupby("Date")[['Game']].max()

    last_game_of_day = b_games.groupby("Date")[['Game']].transform(max)["Game"] == b_games['Game']
    
    plt.subplot(NUM_CHARTS, 1, 1)
    plt.plot(b_games['Date'], b_games['Score'], marker, label=bowler)

    plt.subplot(NUM_CHARTS, 1, 2)
    plt.plot(b_games.loc[last_game_of_day, ['Date']], b_games.loc[last_game_of_day, ['ExMean']], label=bowler)

    plt.subplot(NUM_CHARTS, 1, 3)
    plt.plot(b_games.loc[last_game_of_day, ['Date']], b_games.loc[last_game_of_day, ['RollingAvg10']], label=bowler)

    plt.subplot(NUM_CHARTS, 1, 4)
    plt.plot(b_games.loc[last_game_of_day, ['Date']], b_games.loc[last_game_of_day, ['Handicap']], label=bowler)
    
    plt.subplot(NUM_CHARTS, 1, 5)
    std_per_day = b_games.groupby("Date")[["Score"]].std()
    plt.plot(std_per_day.index, std_per_day["Score"], label=bowler)

    plt.subplot(NUM_CHARTS, 1, 6)
    avg_per_day = b_games.groupby("Date")[["Score"]].mean()
    plt.plot(avg_per_day.index, avg_per_day["Score"], label=bowler)

    plt.subplot(NUM_CHARTS, 1, 7)
    max_per_day = b_games.groupby("Date")[["Score"]].max()
    plt.plot(max_per_day.index, max_per_day["Score"], label=bowler)
    
    bprint(bowler, int(avg), int(bd.loc[bd['Bowler'] == bowler, ["Score"]].max()), int(b_games['RollingAvg10'].tail(1)), int(b_games['Handicap'].tail(1)))

for i in range(NUM_CHARTS):
    plt.subplot(NUM_CHARTS, 1, i+1)
    plt.margins(0.1)
    plt.legend(numpoints=1, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

# plt.show()
fig = plt.figure("Bowling!")
# Make the saved image look a little better
fig.subplots_adjust(bottom=0.005)
fig.set_size_inches(18, 18)
plt.savefig("plots.svg", format="svg", bbox_inches='tight', dpi=1000)
